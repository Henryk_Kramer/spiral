import java.util.ArrayList;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseWheelEvent;

public class Main {

    public static int center_x = 500, center_y = 400;
    public static final int START_DRAWSPEED = 250, DRAWSPEED_CHANGEUNIT = 10;
    public static final int LINE_SPACING = 10;

    public static void main(String[] args) {

        new Window(center_x * 2, center_y * 2, false, Color.BLACK, 1) {

            ArrayList<Color> allColors = new ArrayList<>();
            int drawSpeed = START_DRAWSPEED;
            long pastSeconds = System.currentTimeMillis() + drawSpeed;
            boolean notAtEdge = true;

            @Override
            void draw(Graphics g) {
                if (System.currentTimeMillis() - pastSeconds > drawSpeed) {
                    DrawPosition posStart = new DrawPosition(0, 0, LINE_SPACING);
                    DrawPosition posEnd = new DrawPosition(0, LINE_SPACING, LINE_SPACING);

                    if(notAtEdge) {

                        // generate and save new color
                        allColors.add(randomColor());

                        // draw lines
                        for (int i = 0; i < allColors.size(); i++) {
                            g.setColor(allColors.get(i));
                            g.drawLine(center_x + posStart.x, center_y + posStart.y, center_x + posEnd.x, center_y + posEnd.y);
                            posStart.x = posEnd.x;
                            posStart.y = posEnd.y;
                            posEnd.calcNextPosition();
                            
                        }

                        if(isEdging(posEnd)) notAtEdge = false;

                        pastSeconds = System.currentTimeMillis();
                
                    }

                }
            }

            @Override
            void calculateResizedPosition(int width, int height) {
                center_x = width/2;
                center_y = height/2;
            }

            @Override
            void resetDrawing() {
                allColors.clear();
                notAtEdge = true;
                drawSpeed = START_DRAWSPEED;
            }

            @Override
            void changeDrawSpeed(MouseWheelEvent e) {
                int wheelRotation = e.getWheelRotation();
                if (drawSpeed < START_DRAWSPEED && wheelRotation > 0) {
                    drawSpeed+=DRAWSPEED_CHANGEUNIT;
                } else if (drawSpeed > 40 && wheelRotation < 0) {
                    drawSpeed-=DRAWSPEED_CHANGEUNIT;
                }
            }

            Color randomColor() {
                return new Color((float) Math.random(), (float) Math.random(), (float) Math.random());
            }

            boolean isEdging(DrawPosition d) {
                return (center_x + d.x < 0 || center_y + d.y < 0);
            }

        };

    }

}

//class to hold a position and calculate the next
class DrawPosition {

    int x, y, addedSize;
    boolean stateX;

    DrawPosition(int x, int y, int addedSize) {
        this.x = x;
        this.y = y;
        this.addedSize = addedSize;
        stateX = true;
    }

    void calcNextPosition() {
        if (stateX) {
            if (x > 0) {
                x *= -1;
            } else {
                x *= -1;
                x += addedSize;
            }
            stateX = false;
        } else {
            if (y > 0) {
                y *= -1;
            } else {
                y *= -1;
                y += addedSize;
            }
            stateX = true;
        }
    }

    @Override
    public String toString() {
        return x + " || " + y;
    }

}
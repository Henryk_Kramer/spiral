import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.TimerTask;
import java.util.Timer;


abstract class Window {

    //abstract methods
    abstract void draw(Graphics g);
    abstract void calculateResizedPosition(int width, int height);
    abstract void resetDrawing();
    abstract void changeDrawSpeed(MouseWheelEvent e);

    //variables
    private Frame window;
    private boolean isMaximized;
    private Timer repaintTimer;

    //constructor
    public Window(int width, int height, boolean startInFullscreen, Color backgroundColor, long repaintTime) {

        initializeFrame(); //create Frame
        window.setSize(width, height); //set size
        window.setLocationRelativeTo(null); //center window
        window.setExtendedState((startInFullscreen) ? Frame.MAXIMIZED_BOTH : Frame.NORMAL); //max- or minimizes
        isMaximized = startInFullscreen;
		window.setBackground(backgroundColor); //set background color
        window.setVisible(true); //make window visible
        addListener(); //add input listeners
        initializeRepainter(repaintTime); //repaint the whole window

    }

    //private methods

    private void initializeFrame() {

        window = new Frame("Helix") {
            
            private static final long serialVersionUID = 1L;
					
            @Override
            public void update(Graphics g) {
                paint(g);
            }
            
            @Override													
            public void paint(Graphics g) {			
                super.paint(g);											
                draw(g);
            }
        };

    }

    private void addListener() {

        window.addWindowListener(new WindowListener(){
        
            @Override
            public void windowOpened(WindowEvent e) {}
        
            @Override
            public void windowIconified(WindowEvent e) {}
        
            @Override
            public void windowDeiconified(WindowEvent e) {}
        
            @Override
            public void windowDeactivated(WindowEvent e) {}
        
            @Override
            public void windowClosing(WindowEvent e) {
                window.dispose();
                repaintTimer.cancel();
                repaintTimer.purge();
            }
        
            @Override
            public void windowClosed(WindowEvent e) {}
        
            @Override
            public void windowActivated(WindowEvent e) {}

        });

        window.addMouseListener(new MouseListener(){
        
            @Override
            public void mouseReleased(MouseEvent e) {}
        
            @Override
            public void mousePressed(MouseEvent e) {
                window.getGraphics().clearRect(0, 0, window.getWidth(), window.getHeight());
                resetDrawing();
            }
        
            @Override
            public void mouseExited(MouseEvent e) {}
        
            @Override
            public void mouseEntered(MouseEvent e) {}
        
            @Override
            public void mouseClicked(MouseEvent e) {}

        });

        window.addKeyListener(new KeyListener(){
        
            @Override
            public void keyTyped(KeyEvent e) {
                window.setExtendedState((isMaximized) ? Frame.NORMAL : Frame.MAXIMIZED_BOTH);
                isMaximized = !isMaximized;
            }
        
            @Override
            public void keyReleased(KeyEvent e) {}
        
            @Override
            public void keyPressed(KeyEvent e) {}

        });

        window.addMouseWheelListener(new MouseWheelListener(){
        
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                changeDrawSpeed(e);
            }

        });

        window.addComponentListener(new ComponentListener(){
        
            @Override
            public void componentShown(ComponentEvent e) {}
        
            @Override
            public void componentResized(ComponentEvent e) {
                calculateResizedPosition(window.getWidth(), window.getHeight());
                window.getGraphics().clearRect(0, 0, window.getWidth(), window.getHeight());
            }
        
            @Override
            public void componentMoved(ComponentEvent e) {}
        
            @Override
            public void componentHidden(ComponentEvent e) {}

        });

    }

    private void initializeRepainter(long repaintTime) {

        repaintTimer = new Timer();
        repaintTimer.schedule(new TimerTask(){
        
            @Override
            public void run() {
                window.repaint();
            }

        }, 0, repaintTime);
    }

}
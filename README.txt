Author: Henryk Kramer
Created: Summer 2017
Refactored: February 2019

For school I made a program that simply draws a spiral.

scroll wheel: increase or decrease drawing speed
left mouse button: reset everything in the frame
any key: maxi- or minimize the window